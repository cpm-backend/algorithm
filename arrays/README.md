# Arrays

## Tips
- If you're stuck, think about whether you should be iterating left to right, or right to left, e.g. [Squares of a Sorted Array](leetcode/SquaresOfASortedArray.js), [Replace Elements with Greatest Element on Right Side](leetcode/ReplaceElementsWithGreatestElementOnRightSide.js).
- Use another array to store the original data, e.g. [Merge Sorted Array](leetcode/MergeSortedArray.js).
- Record offset of each element in an array to calculate final position, e.g. [Duplicate Zeros](leetcode/DuplicateZeros.js).
- Use two indices to go through array/arrays, e.g. [Merge Sorted Array](leetcode/MergeSortedArray.js).
- Store data in set for comparison, e.g. [Check If N and Its Double Exist](leetcode/CheckIfNAndItsDoubleExist.js).
- Compare special pattern in array, e.g. [Valid Mountain Array](leetcode/ValidMountainArray.js).
- Use position to record valid data, this skill is usually used in problem that is to delete some elements in array, e.g. [Remove Duplicates from Sorted Array](leetcode/RemoveDuplicatesFromSortedArray.js).
