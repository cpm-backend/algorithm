/**
 * Third Maximum Number
 *
 * Given integer array nums, return the third maximum number in this array. If the third maximum does not exist, return the maximum number.
 *
 * Example:
 * Input: nums = [3,2,1]
 * Output: 1
 * Explanation: The third maximum is 1.
 *
 * @param {number[]} nums
 * @return {number}
 */
const thirdMax = function (nums) {
	nums.sort((a, b) => b - a);
	const length = nums.length;
	let i = 0;
	let rank = 1;
	while (i < length - 1 && rank < 3) {
		if (nums[i] !== nums[i + 1]) {
			rank++;
		}
		i++;
	}
	return rank < 3 ? nums[0] : nums[i];
};

console.log(thirdMax([2, 2, 3, 1])); // 1
