/**
 * Max Consecutive Ones Solution
 *
 * Given a binary array nums, return the maximum number of consecutive 1's in the array.
 *
 * Example:
 * Input: nums = [1,1,0,1,1,1]
 * Output: 3
 * Explanation: The first two digits or the last three digits are consecutive 1s. The maximum number of consecutive 1s is 3.
 *
 * @param {number[]} nums
 * @return {number}
 */
const findMaxConsecutiveOnes = function (nums) {
	let max = 0;
	let length = 0;
	for (let num of nums) {
		if (num === 1) {
			length++;
		} else {
			max = Math.max(max, length);
			length = 0;
		}
	}
	return Math.max(max, length);
};

console.log(findMaxConsecutiveOnes([1, 1, 0, 1, 1, 1])); // 3
