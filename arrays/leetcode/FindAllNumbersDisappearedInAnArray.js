/**
 * Find All Numbers Disappeared in an Array
 *
 * Given an array nums of n integers where nums[i] is in the range [1, n], return an array of all the integers in the range [1, n] that do not appear in nums.
 *
 * Example:
 * Input: nums = [4,3,2,7,8,2,3,1]
 * Output: [5,6]
 *
 * @param {number[]} nums
 * @return {number[]}
 */
const findDisappearedNumbers = function (nums) {
	const length = nums.length;
	const map = {};
	for (let num of nums) {
		map[num] = true;
	}
	const ans = [];
	for (let i = 1; i <= length; i++) {
		if (!map[i]) {
			ans.push(i);
		}
	}
	return ans;
};

console.log(findDisappearedNumbers([1, 1])); // [2]
