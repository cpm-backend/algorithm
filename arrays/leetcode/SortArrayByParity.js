/**
 * Sort Array By Parity
 *
 * Given an array nums of non-negative integers, return an array consisting of all the even elements of nums, followed by all the odd elements of nums.
 * You may return any answer array that satisfies this condition.
 *
 * Example:
 * Input: nums = [3,1,2,4]
 * Output: [2,4,3,1]
 * The outputs [4,2,3,1], [2,4,1,3], and [4,2,1,3] would also be accepted.
 *
 * @param {number[]} nums
 * @return {number[]}
 */
const sortArrayByParity = function (nums) {
	const odds = [];
	const evens = [];
	for (let num of nums) {
		if (num % 2 === 0) {
			evens.push(num);
		} else {
			odds.push(num);
		}
	}
	return evens.concat(odds);
};

console.log(sortArrayByParity([3, 1, 2, 4])); // [2,4,3,1]
