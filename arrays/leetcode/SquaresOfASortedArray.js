/**
 * Squares of a Sorted Array
 *
 * Given an integer array nums sorted in non-decreasing order, return an array of the squares of each number sorted in non-decreasing order.
 *
 * Input: nums = [-4,-1,0,3,10]
 * Output: [0,1,9,16,100]
 * Explanation: After squaring, the array becomes [16,1,0,9,100]. After sorting, it becomes [0,1,9,16,100].
 *
 * @param {number[]} nums
 * @return {number[]}
 */
const sortedSquares = function (nums) {
	let start = 0;
	let end = nums.length - 1;
	let order = end;
	const ans = Array(nums.length).fill(0);
	while (order !== -1) {
		if (Math.abs(nums[end]) > Math.abs(nums[start])) {
			ans[order] = Math.pow(nums[end], 2);
			order--;
			end--;
		} else {
			ans[order] = Math.pow(nums[start], 2);
			order--;
			start++;
		}
	}
	return ans;
};

console.log(sortedSquares([-4, -1, 0, 3, 10])); // [0,1,9,16,100]
