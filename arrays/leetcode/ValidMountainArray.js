/**
 * Valid Mountain Array
 *
 * Given an array of integers arr, return true if and only if it is a valid mountain array.
 * Recall that arr is a mountain array if and only if:
 * arr.length >= 3
 * There exists some i with 0 < i < arr.length - 1 such that:
 * arr[0] < arr[1] < ... < arr[i - 1] < arr[i]
 * arr[i] > arr[i + 1] > ... > arr[arr.length - 1]
 *
 * Example:
 * Input: arr = [2,1]
 * Output: false
 *
 * @param {number[]} arr
 * @return {boolean}
 */
const validMountainArray = function (arr) {
	let length = arr.length;
	if (length < 3 || arr[1] === arr[0]) {
		return false;
	}
	let top = 0;
	for (let i = 2; i < length; i++) {
		if (
			(arr[i] > arr[i - 1] && arr[i - 1] > arr[i - 2]) ||
			(arr[i] < arr[i - 1] && arr[i - 1] < arr[i - 2])
		) {
			continue;
		}
		if (arr[i - 1] > arr[i] && arr[i - 1] > arr[i - 2]) {
			top++;
			if (top > 1) {
				return false;
			}
			continue;
		}
		return false;
	}
	return top === 1;
};

console.log(validMountainArray([3, 5, 5])); // false
