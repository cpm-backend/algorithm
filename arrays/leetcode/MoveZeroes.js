/**
 * Move Zeroes
 *
 * Given an integer array nums, move all 0's to the end of it while maintaining the relative order of the non-zero elements.
 * Note that you must do this in-place without making a copy of the array
 *
 * Example:
 * Input: nums = [0,1,0,3,12]
 * Output: [1,3,12,0,0]
 *
 * @param {number[]} nums
 * @return {void} Do not return anything, modify nums in-place instead.
 */
const moveZeroes = function (nums) {
	let position = 0;
	for (let num of nums) {
		if (num !== 0) {
			nums[position] = num;
			position++;
		}
	}
	for (let i = position; i < nums.length; i++) {
		nums[i] = 0;
	}
};

console.log(moveZeroes([0])); // [0]
