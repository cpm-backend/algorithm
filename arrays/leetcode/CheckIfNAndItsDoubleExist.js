/**
 * Check If N and Its Double Exist
 *
 * Given an array arr of integers, check if there exists two integers N and M such that N is the double of M ( i.e. N = 2 * M).
 * More formally check if there exists two indices i and j such that :
 * i != j
 * 0 <= i, j < arr.length
 * arr[i] == 2 * arr[j]
 *
 * Example:
 * Input: arr = [10,2,5,3]
 * Output: true
 * Explanation: N = 10 is the double of M = 5,that is, 10 = 2 * 5.
 *
 * @param {number[]} arr
 * @return {boolean}
 */
const checkIfExist = function (arr) {
	const set = new Set();
	let i = 0;
	let length = arr.length;
	while (i < length) {
		if (arr[i] % 2 === 0 && (set.has(arr[i] / 2) || set.has(arr[i] * 2))) {
			return true;
		}
		if (arr[i] % 2 !== 0 && set.has(arr[i] * 2)) {
			return true;
		}
		set.add(arr[i]);
		i++;
	}
	return false;
};

console.log(checkIfExist([7, 1, 14, 11])); // true
