/**
 * Duplicate Zeros
 *
 * Given a fixed length array arr of integers, duplicate each occurrence of zero, shifting the remaining elements to the right.
 * Note that elements beyond the length of the original array are not written.
 * Do the above modifications to the input array in place, do not return anything from your function.
 *
 * Example:
 * Input: [1,0,2,3,0,4,5,0]
 * Output: null
 * Explanation: After calling your function, the input array is modified to: [1,0,0,2,3,0,0,4]
 *
 * @param {number[]} arr
 * @return {void} Do not return anything, modify arr in-place instead.
 */
const duplicateZeros = function (arr) {
	let offsets = [];
	let offset = 0;
	let len = arr.length;
	for (let i = 0; i < len; i++) {
		if (arr[i] === 0) {
			offset++;
		}
		offsets.push(offset);
	}

	let ans = Array(len).fill(0);
	for (let i = 0; i < len; i++) {
		ans[i + offsets[i]] = arr[i];
		arr[i] = ans[i];
	}
};

console.log(duplicateZeros([1, 2, 3])); // null
